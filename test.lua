local lte = require "./init"
local lfs = require "lfs"

lte.set_view_path(lfs.currentdir() .. '/views')
print(lte.render("test", { test = "test" }))
