local lte = {}
local lfs = require "lfs"
local view_ext, view_pat, view_path

-- localize functions
local lte_inner_render, fetch_template
local lte_print, html_escape, scoped_eval

function lte.set_view_path(path)
	view_path = path
	if(view_path:sub(view_path:len()) ~= "/") then
		view_path = view_path .. '/'
	end
end
function lte.set_view_ext(ext)
	view_ext = ext
	view_pat = "%." .. ext:gsub("%.", "%%.") .. "$"
end
lte.set_view_path(lfs.currentdir())
lte.set_view_ext("lte.html")

function lte.render(file, vars)
	return lte_inner_render(file, vars)
end

-- Templating
local templates = {
	{
		start = "%{%{%{",
		finish = "%}%}%}",
		fn = nil,
	},
	{
		start = "%{%{!",
		finish = "!%}%}",
		fn = "_",
	},
	{
		start = "%{%{",
		finish = "%}%}",
		fn = "__",
	}
}
lte_inner_render = function(file, vars, sections)
	local layout = nil
	local layout_vars = {}
	local sections = sections or {}
	local current_section = nil
	local output = Output:new()

	-- Templating functions
	function extends(_layout, _vars)
		layout = _layout
		layout_vars = _vars
	end
	function section(section)
		return sections[section] or ""
	end
	vars["extends"] = extends
	vars["section"] = section
	function start_section(section)
		current_section = section
		output:push()
	end
	function end_section()
		sections[current_section] = output:pop()
		current_section = nil
	end
	vars["start_section"] = start_section
	vars["end_section"] = end_section

	function component(file, _vars)
		local _vars = _vars or {}
		setmetatable(_vars, { __index = vars })
		output:output(lte_inner_render(file, _vars))
	end
	vars["component"] = component

	-- Build file path
	file = view_path .. file:gsub("%.", "/")
	if not file:find(view_pat) then
		file = file .. "." .. view_ext
	end

	-- Read file
	local f = io.open(file, 'r')
	if(f == nil) then return nil end
	local str = f:read("*a")

	-- Start templating
	-- This creates a mess to be eval'd
	-- Idea stolen from https://github.com/dannote/lua-template
	local i = 1
	local last = 1
	local fn_str = "_[=[";
	while i < #str do
		local c = str:sub(i, i)

		if(c == "{") then
			local index, inner = fetch_template(str, i)
			if(index ~= nil) then 
				fn_str = fn_str .. str:sub(last, i - 1)
				fn_str = string.format("%s]=] %s _[=[", fn_str, inner)
				i = index - 1
				last = index
			end
		end

		i = i + 1
	end
	fn_str = fn_str .. str:sub(last, i) .. "]=]"

	-- Eval the mess above
	local remainder = scoped_eval(output, fn_str, vars)

	-- If there was a layout then run it back
	if(layout ~= nil) then
		for k,v in pairs(layout_vars) do vars[k] = v end
		remainder = lte_inner_render(layout, vars, sections)
	end

	return remainder
end
fetch_template = function(str, i)
	local out = ""
	local start = i
	local no_match = true
	for t = 1, #templates do
		local template = templates[t]
		local match = str:match("^"..template.start, i)
		if(match) then
			i = i + #match
			start = i
			for j = i, #str do
				local match = str:match("^"..template.finish, j)
				if(match) then
					local finish = j - 1
					if(template.fn) then
						out = string.format("%s(%s)", template.fn, str:sub(start, finish))
					else
						out = str:sub(start, finish)
					end
					i = j + #match
					break
				end
			end
			no_match = false
			break
		end
	end

	if(no_match) then return nil end
	return i, out
end

-- Eval and prints
lte_print = function (output)
	return function(str)
		output:output(str)
	end
end
html_escape = function (output)
	return function(str)
		local escaped = tostring(str or ''):gsub("[\">/<'&]", {
		    ["&"] = "&amp;",
		    ["<"] = "&lt;",
		    [">"] = "&gt;",
		    ['"'] = "&quot;",
		    ["'"] = "&#39;",
		    ["/"] = "&#47;"
		  })
		output:output(escaped)
	end
end
scoped_eval = function (output, str, vars)
	local vars = vars or {}
	local inner = { __index = _G }
	vars._ = lte_print(output)
	vars.__ = html_escape(output)
  setmetatable(vars, inner)
	local fn = loadstring(str)
	setfenv(fn, vars)
	fn()
	return output:pop()
end

-- Output
Output = {}
Output.__index = Output
function Output:new()
   local output = {}
   setmetatable(output, Output)
	 output.outputs = {""}
   return output
end
function Output:output(str)
	local last_output = self.outputs[#self.outputs]
	self.outputs[#self.outputs] = last_output .. (str or "")
end
function Output:push()
	table.insert(self.outputs, "")
end
function Output:pop()
	local last_output = self.outputs[#self.outputs]
	table.remove(self.outputs, #self.outputs)
	return last_output
end

-- Copy table
function copy_from_table(table, from)
	for k,v in pairs(from) do
		if(table[k] == nil) then
			table[k] = v
		end
	end
end

return lte
